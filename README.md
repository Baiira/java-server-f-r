# Face recognition GO SECURI Backend

Backend meant to authenticate a user based on a base64 image and retrieve equipment list.

## Getting started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.


### Prerequisites

You need to have a H2 database console installed and running in order to start the server. Sql scripts are located in src.main.ressources.

You might need gradle to.

Then you can run the server using:

```
gradlew bootRun;
```

Or if you're using Intellij you can do it with spring runner throught JavaServerApplication.

# Built with

Gradle


SpringBoot

H2Database

# License
MIT

# Author

Frédéric DAL-POS initial work