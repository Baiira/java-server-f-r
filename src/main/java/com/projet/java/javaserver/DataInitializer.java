package com.projet.java.javaserver;

import com.projet.java.javaserver.entities.equipment.Equipment;
import com.projet.java.javaserver.entities.equipment.EquipmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class DataInitializer implements ApplicationRunner {
    private EquipmentRepository equipmentRepository;

    @Autowired
    public DataInitializer(EquipmentRepository repository) {
        this.equipmentRepository = repository;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        long count = this.equipmentRepository.count();
        if (count == 0) {
            Map<Integer, Equipment> equipments = new HashMap<>();

            equipments.put(1, new Equipment("Mousquetons", 15));
            equipments.put(2, new Equipment("Gants d'intervention", 10));
            equipments.put(3, new Equipment("Ceintures de sécurité tactique", 20));
            equipments.put(4, new Equipment("Détecteurs de métaux", 25));
            equipments.put(5, new Equipment("Brassards de sécurité", 30));
            equipments.put(7, new Equipment("Lampes torches", 15));
            equipments.put(8, new Equipment("Bandeaux « Agents cynophiles »", 5));
            equipments.put(9, new Equipment("Gilets pare-balles", 12));
            equipments.put(10, new Equipment("Chemises manches courtes", 30));
            equipments.put(11, new Equipment("Blousons", 30));
            equipments.put(12, new Equipment("Coupe-vents", 30));
            equipments.put(13, new Equipment("Talkies walkies", 20));
            equipments.put(14, new Equipment("Kits oreillettes", 10));
            equipments.put(15, new Equipment("Tasers", 5));

            equipments.forEach((id, equipment) -> this.equipmentRepository.save(equipment));
        }
    }
}
