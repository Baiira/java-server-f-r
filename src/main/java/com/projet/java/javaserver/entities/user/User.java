package com.projet.java.javaserver.entities.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class User {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "FIRSTNAME")
    private String firstName;

    @Column(name = "LASTNAME")
    private String lastName;

    @Column(name = "FACETOKEN")
    private String faceToken;

    public User() {}

    public User(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    /**
     * Getter for the id property.
     *
     * @return Integer The current id property.
     */
    public Integer getId() {
        return this.id;
    }

    /**
     * Setter for the id property. ONLY FOR TESTING PURPOSE.
     *
     * @return User The current User for chain operations.
     */
    User setId(Integer id) {
        this.id = id;
        return this;
    }

    /**
     * Getter for the firstName property.
     *
     * @return String The current firstName property.
     */
    public String getFirstName() {
        return this.firstName;
    }

    /**
     * Setter for the firstName property.
     *
     * @return User The current User for chain operations.
     */
    User setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    /**
     * Getter for the lastName property.
     *
     * @return String The current lastName property.
     */
    public String getLastName() {
        return this.lastName;
    }

    /**
     * Setter for the lastName property.
     *
     * @return User The current User for chain operations.
     */
    User setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    /**
     * Getter for the faceToken property.
     *
     * @return String The current faceToken property.
     */
    public String getFaceToken() {
        return this.faceToken;
    }

    /**
     * Setter for the faceToken property.
     *
     * @return User The current User for chain operations.
     */
    User setFaceToken(String faceToken) {
        this.faceToken = faceToken;
        return this;
    }
}
