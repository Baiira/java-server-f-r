package com.projet.java.javaserver.entities.took;

import com.projet.java.javaserver.entities.equipment.Equipment;
import com.projet.java.javaserver.entities.user.User;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table
public class Took {
    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    private User user;

    @ManyToOne
    private Equipment equipment;

    Took () {}

    public Took(User user, Equipment equipment) {
        this.user = user;
        this.equipment = equipment;
    }

    /**
     * Getter for the id property.
     * @return Integer The current id property.
     */
    public Integer getId() {
        return id;
    }

    void setId(Integer id) {
        this.id = id;
    }

    /**
     * Getter for the user property.
     * @return User The current user ame property.
     */
    public User getUser() {
        return user;
    }

    /**
     * Getter for the equipment property.
     * @return Equipment The current equipment property.
     */
    public Equipment getEquipment() {
        return equipment;
    }
}
