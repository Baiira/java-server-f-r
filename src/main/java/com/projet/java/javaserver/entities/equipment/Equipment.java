package com.projet.java.javaserver.entities.equipment;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Table(name = "EQUIPMENT")
@Entity
public class Equipment {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private String name;

    @Column(name = "BASEAVAILABLE")
    private Integer baseAvailable;

    @Column
    private Integer left;

    @Transient
    private Boolean taken;

    Equipment() {}

    public Equipment(String name, Integer left) {
        this.name = name;
        this.baseAvailable = left;
        this.left = left;
        this.taken = false;
    }

    /**
     * Getter for the id property.
     * @return Integer The current id property.
     */
    public Integer getId() {
        return this.id;
    }

    void setId(Integer id) {
        this.id = id;
    }

    /**
     * Getter for the name property.
     * @return String The current name property.
     */
    public String getName() {
        return this.name;
    }

    /**
     * Getter for the baseAvailable property.
     * @return Integer The current baseAvailable property.
     */
    public Integer getBaseAvailable() {
        return this.baseAvailable;
    }

    /**
     * Getter for the left property.
     * @return Integer The current left property.
     */
    public Integer getLeft() {
        return left;
    }

    /**
     * Getter for the taken property.
     * @return Boolean The current taken property.
     */
    public Boolean getTaken() {
        return taken;
    }

    /**
     * Will decrement the left property but never go over its baseAvailable.
     * @return Equipment The current Equipment for chain operations.
     */
    public Equipment takeOne() {
        if (this.left > 0) {
            this.left -= 1;
        }
        return this;
    }

    /**
     * Will increment the left property but never go over its baseAvailable.
     * @return Equipment The current Equipment for chain operations.
     */
    public Equipment returnOne() {
        if (this.left < this.baseAvailable) {
            this.left +=1;
        }
        return this;
    }

    /**
     * Setter for taken property when the Equipment is found in Took for a User.
     */
    public void isTaken() {
        this.taken = true;
    }

    /**
     * Setter for taken property when the Equipment is not found in Took for a User.
     */
    public void notTaken() {
        this.taken = false;
    }
}
