package com.projet.java.javaserver.entities.took;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface TookRepository extends CrudRepository<Took, Integer> {
    Optional<Took> findByUserIdAndEquipmentId(Integer userId, Integer equipmentId);

    Iterable<Took> findByUserId(Integer userId);
}
