package com.projet.java.javaserver.entities.equipment;

import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface EquipmentRepository extends CrudRepository<Equipment, Integer> {}
