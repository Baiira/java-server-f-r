package com.projet.java.javaserver.utils;

public class EncodedImage {
    private String image;

    public EncodedImage() {}

    /**
     * Getter for the image property.
     * @return String The current (base64) image property.
     */
    public String getImage() {
        return this.image;
    }

    /**
     * Setter for the image property.
     */
    public EncodedImage setImage(String image) {
        this.image = image;
        return this;
    }
}
