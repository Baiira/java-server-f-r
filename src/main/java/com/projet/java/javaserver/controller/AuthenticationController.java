package com.projet.java.javaserver.controller;

import com.projet.java.javaserver.api.FacePPAPI;
import com.projet.java.javaserver.api.FacePPConstants;
import com.projet.java.javaserver.api.facepp.FaceWithConfidence;
import com.projet.java.javaserver.controller.errors.AuthenticationMatchingException;
import com.projet.java.javaserver.entities.user.User;
import com.projet.java.javaserver.entities.user.UserRepository;
import com.projet.java.javaserver.utils.EncodedImage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/auth")
public class AuthenticationController {
    private final UserRepository userRepository;
    private FacePPAPI facePPAPI;

    @Autowired
    public AuthenticationController(UserRepository userRepository) {
        this.userRepository = userRepository;
        this.facePPAPI = new FacePPAPI();
    }

    /**
     * Url: serverurl:4000/auth/
     * Method: POST
     *
     * Will call a third party API that will recognize the biggest face on the given image.
     * If it does and match the minimum required confidence score and the User is register on the
     * database then the login is successful.
     *
     * @param image EncodedImage The base64 encoded image.
     * @return Optional<User> The user found with a certain confidence.
     * @throws AuthenticationMatchingException If the returned confidence does not match a minimum.
     */
    @PostMapping("/")
    @ResponseBody
    public Optional<User> logMe(@RequestBody EncodedImage image) throws AuthenticationMatchingException {
        FaceWithConfidence found = this.facePPAPI.detectAndFindAgent(image.getImage());
        if (found.getConfidence() > FacePPConstants.MINIMUM_CONFIDENCE) {
            return this.userRepository.findByFaceToken(found.getFace_token());
        } else {
            throw new AuthenticationMatchingException();
        }
    }
}
