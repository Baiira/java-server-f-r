package com.projet.java.javaserver.controller.htcpcp;

import com.projet.java.javaserver.controller.htcpcp.classes.Coffee;
import com.projet.java.javaserver.controller.htcpcp.error.CoffeeAlcoholTypeException;
import com.projet.java.javaserver.controller.htcpcp.error.CoffeeAllTypeException;
import com.projet.java.javaserver.controller.htcpcp.error.CoffeeHeaderException;
import com.projet.java.javaserver.controller.htcpcp.error.CoffeeMilkTypeException;
import com.projet.java.javaserver.controller.htcpcp.error.CoffeeSyrupTypeException;
import com.projet.java.javaserver.controller.htcpcp.error.NotATeaPotException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static com.projet.java.javaserver.controller.htcpcp.CoffeeConstants.ADDITION_TYPE;
import static com.projet.java.javaserver.controller.htcpcp.CoffeeConstants.ALCOHOL_TYPE;
import static com.projet.java.javaserver.controller.htcpcp.CoffeeConstants.ALLOWED_ADDITION_TYPES;
import static com.projet.java.javaserver.controller.htcpcp.CoffeeConstants.ALLOWED_ALCOHOL_TYPES;
import static com.projet.java.javaserver.controller.htcpcp.CoffeeConstants.ALLOWED_MILK_TYPES;
import static com.projet.java.javaserver.controller.htcpcp.CoffeeConstants.ALLOWED_SYRUP_TYPES;
import static com.projet.java.javaserver.controller.htcpcp.CoffeeConstants.ALL_TYPE;
import static com.projet.java.javaserver.controller.htcpcp.CoffeeConstants.BEURK;
import static com.projet.java.javaserver.controller.htcpcp.CoffeeConstants.MILK_TYPE;
import static com.projet.java.javaserver.controller.htcpcp.CoffeeConstants.SYRUP_TYPE;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/coffee")
public class CoffeeController {

    /**
     * Brew the perfect coffee for everyone that gently asks for one.
     * @param additions String The list of what you want in the coffee.
     * @return DeferredResult<ResponseEntity<Coffee>> Because the perfect coffee may take some time...
     * @throws Exception If you've malformed your wish, like asking for tea... Beeeeeeurk.
     */
    @PostMapping
    public DeferredResult<ResponseEntity<Coffee>> brewCoffee(@RequestHeader("Accept-Additions") String additions)
            throws Exception {
        Map<String, String> coffeeHeaders = this.formatHeaders(additions);
        this.checkCoffeeHeaderValidity(coffeeHeaders);
        String additionType = this.isTypeSetAndValid(coffeeHeaders,
                                                     ADDITION_TYPE,
                                                     ALLOWED_ADDITION_TYPES,
                                                     CoffeeHeaderException.class);

        Map<String, String> additionList = new HashMap<>();

        this.formatList(additionType, coffeeHeaders, additionList);

        DeferredResult<ResponseEntity<Coffee>> result = new DeferredResult<>();

        new Thread(() -> result.setResult(ResponseEntity.ok(new Coffee(additionList))), "CoffeePot").start();

        return result;
    }

    private void formatList(String additionType, Map<String, String> headerList, Map<String, String> additionList) {
        headerList.forEach((key, val) -> {
            if (Objects.equals(additionType, ALL_TYPE)) {
                additionList.put(key, val);
            }
            if (Objects.equals(key, additionType)) {
                additionList.put(key, val);
            }
        });
    }

    private void checkCoffeeHeaderValidity(Map<String, String> coffeeHeaders) throws Exception {
        String additionType = isTypeSetAndValid(coffeeHeaders,
                                                ADDITION_TYPE,
                                                ALLOWED_ADDITION_TYPES,
                                                CoffeeHeaderException.class);
        if (!this.isAdditionTypeValid(additionType)) throw new CoffeeHeaderException();

        switch (additionType) {
            case ALCOHOL_TYPE:
                isTypeSetAndValid(coffeeHeaders, ALCOHOL_TYPE, ALLOWED_ALCOHOL_TYPES, CoffeeAlcoholTypeException.class);
                break;
            case SYRUP_TYPE:
                isTypeSetAndValid(coffeeHeaders, SYRUP_TYPE, ALLOWED_SYRUP_TYPES, CoffeeSyrupTypeException.class);
                break;
            case MILK_TYPE:
                isTypeSetAndValid(coffeeHeaders, MILK_TYPE, ALLOWED_MILK_TYPES, CoffeeMilkTypeException.class);
                break;
            case ALL_TYPE:
                isTypeSetAndValid(coffeeHeaders, ALCOHOL_TYPE, ALLOWED_ALCOHOL_TYPES, CoffeeAllTypeException.class);
                isTypeSetAndValid(coffeeHeaders, SYRUP_TYPE, ALLOWED_SYRUP_TYPES, CoffeeAllTypeException.class);
                isTypeSetAndValid(coffeeHeaders, MILK_TYPE, ALLOWED_MILK_TYPES, CoffeeAllTypeException.class);
                break;
        }
    }

    private String isTypeSetAndValid(Map<String, String> coffeeHeaders,
                                     String p,
                                     String[] allowedTypeList,
                                     Class<? extends Exception> e) throws Exception {
        if (!coffeeHeaders.containsKey(p)) {
            throw e.getDeclaredConstructor().newInstance();
        } else {
            String value = coffeeHeaders.get(p);
            if (this.isPresentInList(value, allowedTypeList)) {
                return value;
            }
            throw e.getDeclaredConstructor().newInstance();
        }
    }

    private Boolean isPresentInList(String value, String[] allowedTypeList) {
        for (String typeAllowed : allowedTypeList) {
            if (Objects.equals(value, typeAllowed)) {
                return true;
            }
        }
        return false;
    }

    private Boolean isAdditionTypeValid(String additionType) {
        return Objects.equals(additionType, ALL_TYPE) || Objects.equals(additionType, MILK_TYPE) || Objects.equals(
                additionType,
                SYRUP_TYPE) || Objects.equals(additionType, ALCOHOL_TYPE);
    }

    private Map<String, String> formatHeaders(String header) {
        String[] allValues = header.split(";");
        Map<String, String> headers = new HashMap<>();

        if (allValues.length < 1) throw new CoffeeHeaderException();

        for (String value : allValues) {
            String[] keyValue = value.split("=");
            if (keyValue[0].isEmpty() || keyValue[1].isEmpty()) throw new CoffeeHeaderException();
            if (keyValue[0].matches(BEURK) || keyValue[1].matches(BEURK)) throw new NotATeaPotException();
            headers.put(keyValue[0], keyValue[1]);
        }
        return headers;
    }
}
