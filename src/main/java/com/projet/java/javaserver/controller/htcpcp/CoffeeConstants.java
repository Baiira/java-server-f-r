package com.projet.java.javaserver.controller.htcpcp;

public class CoffeeConstants {
    public static final Integer NOT_A_TEA_POT_ERROR_STATUS = 481;

    public static final String ADDITION_TYPE = "addition-type";
    public static final String MILK_TYPE = "milk-type";
    public static final String SYRUP_TYPE = "syrup-type";
    public static final String ALCOHOL_TYPE = "alcohol-type";
    static final String ALL_TYPE = "*";
    static final String BEURK = "tea";

    private static final String MILK_TYPE_CREAM = "Cream";
    private static final String MILK_TYPE_HALF = "Half-and-half";
    private static final String MILK_TYPE_WHOLE = "Whole-milk";
    private static final String MILK_TYPE_PART = "Part-Skim";
    private static final String MILK_TYPE_SKIM = "Skim";
    private static final String MILK_TYPE_NON_DIARY = "Non-Dairy";

    private static final String SYRUP_TYPE_VANILLA = "Vanilla";
    private static final String SYRUP_TYPE_ALMOND = "Almond";
    private static final String SYRUP_TYPE_RASPBERRY = "Raspberry";
    private static final String SYRUP_TYPE_CHOCOLATE = "Chocolate";

    private static final String ALCOHOL_TYPE_WHISKY = "Whisky";
    private static final String ALCOHOL_TYPE_RUM = "Rum";
    private static final String ALCOHOL_TYPE_KAHLUA = "Kahlua";
    private static final String ALCOHOL_TYPE_AQUAVIT = "Aquavit";

    public static final String[] ALLOWED_ADDITION_TYPES = {MILK_TYPE, SYRUP_TYPE, ALCOHOL_TYPE, ALL_TYPE};
    public static final String[] ALLOWED_ALCOHOL_TYPES = {ALCOHOL_TYPE_WHISKY, ALCOHOL_TYPE_RUM, ALCOHOL_TYPE_KAHLUA,
            ALCOHOL_TYPE_AQUAVIT};
    public static final String[] ALLOWED_SYRUP_TYPES = {SYRUP_TYPE_VANILLA, SYRUP_TYPE_RASPBERRY, SYRUP_TYPE_ALMOND,
            SYRUP_TYPE_CHOCOLATE};
    public static final String[] ALLOWED_MILK_TYPES = {MILK_TYPE_CREAM, MILK_TYPE_HALF, MILK_TYPE_WHOLE, MILK_TYPE_PART,
            MILK_TYPE_SKIM, MILK_TYPE_NON_DIARY};
}
