package com.projet.java.javaserver.controller.htcpcp.error;

public class CoffeeMilkTypeException extends RuntimeException {
    public CoffeeMilkTypeException() {super("COFFEE_MILK_TYPE_EXCEPTION");}
}
