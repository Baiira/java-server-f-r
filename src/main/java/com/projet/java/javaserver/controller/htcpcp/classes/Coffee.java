package com.projet.java.javaserver.controller.htcpcp.classes;

import java.util.Map;

import static com.projet.java.javaserver.controller.htcpcp.CoffeeConstants.ADDITION_TYPE;
import static com.projet.java.javaserver.controller.htcpcp.CoffeeConstants.ALCOHOL_TYPE;
import static com.projet.java.javaserver.controller.htcpcp.CoffeeConstants.MILK_TYPE;
import static com.projet.java.javaserver.controller.htcpcp.CoffeeConstants.SYRUP_TYPE;

public class Coffee {
    private String coffee_served;

    /**
     * Takes a Map of String, String formatted as TYPE, VALUE and serve your perfect coffee.
     * @param typesAsked Map<String, String> All you want in your coffee.
     */
    public Coffee(Map<String, String> typesAsked) {
        this.coffee_served = "Here is your coffee with";
        typesAsked.remove(ADDITION_TYPE);
        typesAsked.forEach((key, val) -> {
            coffee_served = coffee_served.replace("%s", " ")
                    .replace("%v", "")
                    .replace("%&", ",");

            switch (key) {
                case MILK_TYPE:
                    coffee_served += "%sa small touch of " + val.toLowerCase() + "%&%v";
                    break;
                case SYRUP_TYPE:
                    coffee_served += "%sa swig of " + val.toLowerCase() + "%&%v";
                    break;
                case ALCOHOL_TYPE:
                    coffee_served += "%sthat delicious " + val.toLowerCase() + "%&%v";
                    break;
            }
        });
        if (typesAsked.size() == 1) {
            coffee_served = coffee_served.replace("%s", " ");
        }
        coffee_served = coffee_served.replace(",%s", " and ")
                .replace("%&%v", ".");
    }

    /**
     * Getter for coffee_served property.
     * @return String The current coffee_served property.
     */
    public String getCoffee_served() {
        return coffee_served;
    }
}
