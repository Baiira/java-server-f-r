package com.projet.java.javaserver.controller.htcpcp.error;

public class CoffeeHeaderException extends RuntimeException {
    public CoffeeHeaderException() {super("COFFEE_HEADERS_MISMATCH");}
}

