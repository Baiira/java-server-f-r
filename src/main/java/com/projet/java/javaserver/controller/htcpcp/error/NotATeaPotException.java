package com.projet.java.javaserver.controller.htcpcp.error;

public class NotATeaPotException extends RuntimeException {
    public NotATeaPotException() {super("NOT_A_TEA_POT_EXCEPTION");}
}
