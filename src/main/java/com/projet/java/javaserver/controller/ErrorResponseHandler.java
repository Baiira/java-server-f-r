package com.projet.java.javaserver.controller;

import com.projet.java.javaserver.controller.errors.AuthenticationMatchingException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@RestController
public class ErrorResponseHandler extends ResponseEntityExceptionHandler {
    /**
     * Basic handler for no AuthenticationMatchingException.
     * Will send a 404 NOT_FOUND, the error message and the stack trace.
     *
     * @param ex AuthenticationMatchingException The no matching exception.
     *
     * @return ResponseEntity With the error message and a 404 NOT_FOUND.
     */
    @ExceptionHandler(AuthenticationMatchingException.class)
    @ResponseBody
    public final ResponseEntity<String> handleNoMatchingFoundException(
            AuthenticationMatchingException ex) {
        return new ResponseEntity<>(ex.getLocalizedMessage(), HttpStatus.NOT_FOUND);
    }

}
