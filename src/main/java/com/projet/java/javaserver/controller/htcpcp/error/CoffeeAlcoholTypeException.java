package com.projet.java.javaserver.controller.htcpcp.error;

public class CoffeeAlcoholTypeException extends RuntimeException {
    public CoffeeAlcoholTypeException() {super("COFFEE_ALCOHOL_TYPE_EXCEPTION");}
}
