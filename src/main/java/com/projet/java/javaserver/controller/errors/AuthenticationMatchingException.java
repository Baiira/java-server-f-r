package com.projet.java.javaserver.controller.errors;

public class AuthenticationMatchingException extends RuntimeException {
    public AuthenticationMatchingException() {
        super("NO_MATCHING_USER_FOUND_EXCEPTION");
    }
}
