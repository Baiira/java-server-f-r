package com.projet.java.javaserver.controller;

import com.projet.java.javaserver.entities.equipment.Equipment;
import com.projet.java.javaserver.entities.equipment.EquipmentRepository;
import com.projet.java.javaserver.entities.took.Took;
import com.projet.java.javaserver.entities.took.TookRepository;
import com.projet.java.javaserver.entities.user.User;
import com.projet.java.javaserver.entities.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Objects;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/equipment")
public class EquipmentController {
    private final EquipmentRepository equipmentRepository;
    private final TookRepository tookRepository;
    private final UserRepository userRepository;

    @Autowired
    public EquipmentController(EquipmentRepository equipmentRepository,
                               TookRepository tookRepository,
                               UserRepository userRepository) {
        this.equipmentRepository = equipmentRepository;
        this.tookRepository = tookRepository;
        this.userRepository = userRepository;
    }

    /**
     * Url: serverurl:4000/equipment/byuser/{userId}
     * Method: Get
     *
     * Will get from the database the Took with the given userId.
     * From that it will pass the flag taken to true for all the Equipments found from the Took table.
     *
     * @param userId Integer The id for the user that might have took Equipments.
     * @return Iterable<Equipment> The list with all Equipments and an indicator if they have been took by the user.
     */
    @GetMapping("/byuser/{userId}")
    @ResponseBody
    public Iterable<Equipment> getAllEquipments(@PathVariable("userId") Integer userId) {
        Iterable<Took> tooks = this.tookRepository.findByUserId(userId);
        Iterable<Equipment> equipments = this.equipmentRepository.findAll();
        tooks.forEach(took -> equipments.forEach(equipment -> {
            if (Objects.equals(took.getEquipment().getId(), equipment.getId())) {
                equipment.isTaken();
            }
        }));
        return equipments;
    }

    /**
     * Url: serverurl:4000/equipment/take/{id}/{userId}
     * Method: Put
     *
     * Will get from database the Equipment with the given id.
     * From that, it will look for the User with the given id.
     * If those are found it will decrement the left property for the Equipment,
     * and bind the User and the Equipment too a third table called Took.
     *
     * @param id Integer The id for the Equipment that need to be taken.
     * @param userId Integer The id for the User that takes the Equipment.
     * @return Optional<Equipment> The Equipment updated.
     */
    @PutMapping("/take/{id}/{userId}")
    @ResponseBody
    public Optional<Equipment> takeOneFor(@PathVariable(value = "id") Integer id,
                                          @PathVariable("userId") Integer userId) {
        Optional<Equipment> equipment = this.equipmentRepository.findById(id);
        equipment.ifPresent(Equipment::takeOne);
        equipment.ifPresent(this.equipmentRepository::save);

        equipment.ifPresent(equipment1 -> {
            Optional<User> u = this.userRepository.findById(userId);
            u.ifPresent(user -> {
                Took t = new Took(user, equipment1);
                this.tookRepository.save(t);
            });
        });

        return equipment;
    }

    /**
     * Url: serverurl:4000/equipment/return/{id}/{userId}
     * Method: Put
     *
     * Will get from database the Equipment with the given id.
     * From that, it will look for the User with the given id.
     * If those are found it will increment the left property for the Equipment,
     * and remove from Took table their association.
     *
     * @param id Integer The id for the Equipment that need to be taken.
     * @param userId Integer The id for the User that return the Equipment.
     * @return Optional<Equipment> The Equipment updated.
     */
    @DeleteMapping("/return/{id}/{userId}")
    @ResponseBody
    public Optional<Equipment> returnOneFor(@PathVariable(value = "id") Integer id,
                                            @PathVariable("userId") Integer userId) {
        Optional<Equipment> equipment = this.equipmentRepository.findById(id);
        equipment.ifPresent(Equipment::returnOne);
        equipment.ifPresent(this.equipmentRepository::save);

        equipment.ifPresent(equipment1 -> {
            Optional<User> u = this.userRepository.findById(userId);
            u.ifPresent(user -> {
                Optional<Took> t = this.tookRepository.findByUserIdAndEquipmentId(userId, equipment1.getId());
                t.ifPresent(this.tookRepository::delete);
            });
        });

        return equipment;
    }
}
