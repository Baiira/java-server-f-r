package com.projet.java.javaserver.controller.htcpcp.error;

import com.projet.java.javaserver.controller.htcpcp.classes.CoffeeError;
import com.projet.java.javaserver.controller.htcpcp.classes.CoffeeErrorWithAllowed;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import static com.projet.java.javaserver.controller.htcpcp.CoffeeConstants.ALLOWED_ADDITION_TYPES;
import static com.projet.java.javaserver.controller.htcpcp.CoffeeConstants.ALLOWED_ALCOHOL_TYPES;
import static com.projet.java.javaserver.controller.htcpcp.CoffeeConstants.ALLOWED_MILK_TYPES;
import static com.projet.java.javaserver.controller.htcpcp.CoffeeConstants.ALLOWED_SYRUP_TYPES;
import static com.projet.java.javaserver.controller.htcpcp.CoffeeConstants.NOT_A_TEA_POT_ERROR_STATUS;


@ControllerAdvice
@RestController
public class CoffeeErrorResponseHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(CoffeeAlcoholTypeException.class)
    @ResponseBody
    public ResponseEntity<CoffeeErrorWithAllowed> handleAlcoholTypeException(CoffeeAlcoholTypeException ex) {
        return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
                .body(new CoffeeErrorWithAllowed(HttpStatus.NOT_ACCEPTABLE, ex.getLocalizedMessage(), ALLOWED_ALCOHOL_TYPES));
    }

    @ExceptionHandler(CoffeeSyrupTypeException.class)
    @ResponseBody
    public ResponseEntity<CoffeeError> handleSyrupTypeException(CoffeeSyrupTypeException ex) {
        return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
                .body(new CoffeeErrorWithAllowed(HttpStatus.NOT_ACCEPTABLE, ex.getLocalizedMessage(), ALLOWED_SYRUP_TYPES));
    }

    @ExceptionHandler(CoffeeMilkTypeException.class)
    @ResponseBody
    public ResponseEntity<CoffeeError> handleMilkTypeException(CoffeeMilkTypeException ex) {
        return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
                .body(new CoffeeErrorWithAllowed(HttpStatus.NOT_ACCEPTABLE, ex.getLocalizedMessage(), ALLOWED_MILK_TYPES));
    }

    @ExceptionHandler(CoffeeHeaderException.class)
    @ResponseBody
    public ResponseEntity<CoffeeError> handleCoffeeHeaderException(CoffeeHeaderException ex) {
        return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
                .body(new CoffeeErrorWithAllowed(HttpStatus.NOT_ACCEPTABLE, ex.getLocalizedMessage(), ALLOWED_ADDITION_TYPES));
    }

    @ExceptionHandler(CoffeeAllTypeException.class)
    @ResponseBody
    public ResponseEntity<CoffeeErrorWithAllowed> handleCoffeeAllTypeException(CoffeeAllTypeException ex) {
        return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE)
                .body(new CoffeeErrorWithAllowed(HttpStatus.NOT_ACCEPTABLE, ex.getLocalizedMessage(),
                                                 "Should contain all types!", ALLOWED_ADDITION_TYPES));
    }

    @ExceptionHandler(NotATeaPotException.class)
    @ResponseBody
    public ResponseEntity handleNotATeaPotException(NotATeaPotException ex) {
        return ResponseEntity.status(NOT_A_TEA_POT_ERROR_STATUS).body(new CoffeeError(NOT_A_TEA_POT_ERROR_STATUS, ex.getLocalizedMessage()));
    }
}
