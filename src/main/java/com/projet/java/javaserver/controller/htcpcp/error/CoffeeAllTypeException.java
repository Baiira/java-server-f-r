package com.projet.java.javaserver.controller.htcpcp.error;

public class CoffeeAllTypeException extends RuntimeException {
    public CoffeeAllTypeException() {super("COFFEE_ALL_TYPE_EXCEPTION");}
}
