package com.projet.java.javaserver.controller.htcpcp.classes;

import org.springframework.http.HttpStatus;

public class CoffeeErrorWithAllowed extends CoffeeError {
    private String[] allowed_header_types;

    public CoffeeErrorWithAllowed(HttpStatus httpStatus, String errorMessage) {
        super(httpStatus, errorMessage, "NO_ALLOWED_TYPES_AVAILABLE");
    }

    public CoffeeErrorWithAllowed(HttpStatus httpStatus, String errorMessage, String[] allowedTypes) {
        super(httpStatus, errorMessage);
        this.allowed_header_types = allowedTypes;
    }

    public CoffeeErrorWithAllowed(HttpStatus httpStatus, String errorMessage, String message,String[] allowedTypes) {
        super(httpStatus, errorMessage, message);
        this.allowed_header_types = allowedTypes;
    }

    public CoffeeErrorWithAllowed(Integer httpStatus, String errorMessage) {
        super(httpStatus, errorMessage, "NO_ALLOWED_TYPES_AVAILABLE");
    }

    public CoffeeErrorWithAllowed(Integer httpStatus, String errorMessage, String[] allowedTypes) {
        super(httpStatus, errorMessage);
        this.allowed_header_types = allowedTypes;
    }

    public CoffeeErrorWithAllowed(Integer httpStatus, String errorMessage, String message,String[] allowedTypes) {
        super(httpStatus, errorMessage, message);
        this.allowed_header_types = allowedTypes;
    }

    /**
     * Getter for the allowed_header_property property.
     * @return String[] The current allowed_header_property property.
     */
    public String[] getAllowed_header_types() {
        return this.allowed_header_types;
    }
}
