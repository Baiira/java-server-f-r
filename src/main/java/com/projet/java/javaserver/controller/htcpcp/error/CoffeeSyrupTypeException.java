package com.projet.java.javaserver.controller.htcpcp.error;

public class CoffeeSyrupTypeException extends RuntimeException {
    public CoffeeSyrupTypeException() {super("COFFEE_SYRUP_TYPE_EXCEPTION");}
}
