package com.projet.java.javaserver.controller.htcpcp.classes;

import org.springframework.http.HttpStatus;

import java.util.Date;

public class CoffeeError {
    private Date timestamp;
    private Integer error_status;
    private String error_message;
    private String message;
    private String path = "/coffee";

    CoffeeError(HttpStatus httpStatus, String errorMessage) {
        this.error_status = httpStatus.value();
        this.message = httpStatus.getReasonPhrase();
        this.error_message = errorMessage;
        this.timestamp = new Date();
    }

    public CoffeeError(Integer errorStatus, String errorMessage) {
        this.error_status = errorStatus;
        this.message = "I'm not a tea pot!";
        this.error_message = errorMessage;
        this.timestamp = new Date();
    }

    CoffeeError(Integer errorStatus, String errorMessage, String message) {
        this.error_status = errorStatus;
        this.message = message;
        this.error_message = errorMessage;
        this.timestamp = new Date();
    }

    CoffeeError(HttpStatus httpStatus, String errorMessage, String message) {
        this.error_status = httpStatus.value();
        this.message = message;
        this.error_message = errorMessage;
        this.timestamp = new Date();
    }

    /**
     * Getter for the error_status property.
     * @return Integer The current error_status property.
     */
    public Integer getError_status() {
        return error_status;
    }

    /**
     * Getter for the message property.
     * @return String The current message property.
     */
    public String getMessage() {
        return message;
    }

    /**
     * Getter for the timestamp property.
     * @return Date The timestamp error_status property.
     */
    public Date getTimestamp() {
        return timestamp;
    }

    /**
     * Getter for the error_message property.
     * @return String The current error_message property.
     */
    public String getError_message() {
        return this.error_message;
    }

    /**
     * Getter for the path property.
     * @return String The current path property.
     */
    public String getPath() {
        return this.path;
    }
}
