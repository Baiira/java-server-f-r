package com.projet.java.javaserver.api;

import com.projet.java.javaserver.api.facepp.DetectResponse;
import com.projet.java.javaserver.api.facepp.DetectionException;
import com.projet.java.javaserver.api.facepp.FaceWithConfidence;
import com.projet.java.javaserver.api.facepp.SearchResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

@Service
public class FacePPAPI {

    private CallAPI callAPI;

    public FacePPAPI() {
        this.callAPI = new CallAPI();
    }

    /**
     * Will call to a the Face++ API endpoints (Detect then Search).
     * The goal is to find the closest matching face from the one detect in the given image.
     * @param base64Image String The base64 image for analysis.
     * @return FaceWithConfidence The closest matching face with it's confidence score.
     */
    public FaceWithConfidence detectAndFindAgent(String base64Image) {

        DetectResponse detectResult = this.callDetectAPI(base64Image);

        if (detectResult.getFaces().length < 1) {
            throw new DetectionException();
        }
        
        return this.callSearchAPIForAgent(detectResult.getFaces()[0].getFace_token()).getResults()[0];
    }

    private DetectResponse callDetectAPI(String base64Image) {
        MultiValueMap<String, String> headers = this.buildHeadersWithKeys();
        MultiValueMap<String, String> body = this.buildBodyWithKeys();
        body.set(FacePPConstants.IMAGE_B64_PARAM, base64Image);

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(body, headers);

        return this.callAPI.sendPost(FacePPConstants.FACEPP_DETECT, request, DetectResponse.class);
    }

//    private JSONPObject callFaceSetAddForAgent(String faceToken) {
//        MultiValueMap<String, String> headers = this.buildHeadersWithKeys();
//        headers.set(FacePPConstants.API_FACE_TOKENS_PARAM, faceToken);
//        headers.set(FacePPConstants.API_FACE_SET_PARAM, FacePPConstants.AGENT_FACE_SET_TOKEN);
//
//        HttpEntity<String> request = new HttpEntity<>(headers);
//
//        return this.callAPI.sendPost(FacePPConstants.FACEPP_FACESET_ADD, request);
//    }

    private SearchResponse callSearchAPIForAgent(String faceToken) {
        MultiValueMap<String, String> headers = this.buildHeadersWithKeys();
        MultiValueMap<String, String> body = this.buildBodyWithKeys();
        body.set(FacePPConstants.API_FACE_TOKEN_PARAM, faceToken);
        body.set(FacePPConstants.API_FACE_SET_PARAM, FacePPConstants.AGENT_FACE_SET_TOKEN);

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(body, headers);

        return this.callAPI.sendPost(FacePPConstants.FACEPP_SEARCH, request, SearchResponse.class);
    }

    private MultiValueMap<String, String> buildHeadersWithKeys() {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();

        headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED_VALUE);

        return headers;
    }

    private MultiValueMap<String, String> buildBodyWithKeys() {
        MultiValueMap<String, String> body = new LinkedMultiValueMap<>();

        body.set(FacePPConstants.API_KEY_PARAM, FacePPConstants.API_KEY);
        body.set(FacePPConstants.API_SECRET_PARAM, FacePPConstants.API_SECRET);

        return body;
    }
}
