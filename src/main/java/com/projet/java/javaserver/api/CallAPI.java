package com.projet.java.javaserver.api;

import org.springframework.http.HttpEntity;
import org.springframework.web.client.RestTemplate;

class CallAPI {
    /**
     * Default POST sender in order to make HTTP POST calls to APIs.
     * @param url String The url for the call.
     * @param request HttpEntity The headers and body to send to the API.
     * @param c Class The Class for the expected response for the call.
     * @param <T> The type of the Object for the response.
     * @return <T> The formatted response from the API.
     */
    <T> T sendPost(String url, HttpEntity request, Class<T> c) {
        RestTemplate restTemplate = new RestTemplate();
        return restTemplate.postForObject(url, request, c);
    }
}
