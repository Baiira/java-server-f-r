package com.projet.java.javaserver.api;

public class FacePPConstants {
    private final static String FACEPP_URL = "https://api-us.faceplusplus.com/facepp/v3/";
    private final static String FACEPP_FACESET = FacePPConstants.FACEPP_URL + "faceset/";

    static final String FACEPP_SEARCH = FacePPConstants.FACEPP_URL + "search";
    static final String FACEPP_DETECT = FacePPConstants.FACEPP_URL + "detect";
    public final static String FACEPP_FACESET_ADD = FacePPConstants.FACEPP_FACESET + "addface";

    static final String API_KEY = "API_KEY";
    static final String API_SECRET = "API_SECRET";

    static final String AGENT_FACE_SET_TOKEN = "FACE_SET_TOKEN";

    static final String IMAGE_B64_PARAM = "image_base64";
    static final String API_KEY_PARAM = "api_key";
    static final String API_SECRET_PARAM = "api_secret";
    static final String API_FACE_TOKEN_PARAM = "face_token";
    public final static String API_FACE_TOKENS_PARAM = "face_tokens";
    static final String API_FACE_SET_PARAM = "faceset_token";

    public final static Float MINIMUM_CONFIDENCE = 80F;
}
