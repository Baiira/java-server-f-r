package com.projet.java.javaserver.api.facepp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchResponse {
    private FaceWithConfidence[] results;

    public SearchResponse() {}

    public SearchResponse(FaceWithConfidence[] results) {
        this.results = results;
    }

    /**
     * Getter for the results property.
     * @return FaceWithConfidence The current results property.
     */
    public FaceWithConfidence[] getResults() {
        return this.results;
    }

    /**
     * Setter for the results property.
     */
    public void setResults(FaceWithConfidence[] results) {
        this.results = results;
    }
}
