package com.projet.java.javaserver.api.facepp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Faces {
    private String face_token;

    public Faces(){}

    public Faces(String face_token) {
        this.face_token = face_token;
    }


    /**
     * Getter for the face_token property.
     * @return String The current face_token property.
     */
    public String getFace_token() {
        return this.face_token;
    }

    /**
     * Setter for the face_token property.
     */
    public void setFace_token(String face_token) {
        this.face_token = face_token;
    }
}
