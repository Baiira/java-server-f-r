package com.projet.java.javaserver.api.facepp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DetectResponse {
    private Faces faces[];

    public DetectResponse() {}

    public DetectResponse(Faces[] faces) {
        this.faces = faces;
    }

    /**
     * Getter for the faces property.
     * @return Faces[] The current faces property.
     */
    public Faces[] getFaces() {
        return this.faces;
    }

    /**
     * Setter for the faces property.
     */
    public void setFaces(Faces[] faces) {
        this.faces = faces;
    }
}

