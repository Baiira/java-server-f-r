package com.projet.java.javaserver.api.facepp;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FaceWithConfidence {
    private String face_token;
    private Float confidence;

    public FaceWithConfidence() {}

    public FaceWithConfidence(String face_token, Float confidence) {
        this.face_token = face_token;
        this.confidence = confidence;
    }

    /**
     * Getter for the face_token property.
     * @return String The current face_token property.
     */
    public String getFace_token() {
        return this.face_token;
    }

    /**
     * Setter for the face_token property.
     */
    public void setFace_token(String face_token) {
        this.face_token = face_token;
    }

    /**
     * Getter for the confidence property.
     * @return Float The current confidence property.
     */
    public Float getConfidence() {
        return this.confidence;
    }

    /**
     * Setter for the face_token property.
     */
    public void setConfidence(Float confidence) {
        this.confidence = confidence;
    }
}