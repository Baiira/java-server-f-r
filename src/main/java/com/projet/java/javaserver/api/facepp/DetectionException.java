package com.projet.java.javaserver.api.facepp;

public class DetectionException extends RuntimeException{
    public DetectionException() {
        super("NO_FACES_DETECTED_EXCEPTION");
    }
}
