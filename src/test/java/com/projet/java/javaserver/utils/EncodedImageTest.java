package com.projet.java.javaserver.utils;

import org.junit.Test;

import static org.junit.Assert.*;

public class EncodedImageTest {

    @Test
    public void setAndGetImage() {
        String fakeB64image = "coucou";
        EncodedImage image = new EncodedImage();
        image.setImage(fakeB64image);
        assertSame(fakeB64image, image.getImage());
    }
}