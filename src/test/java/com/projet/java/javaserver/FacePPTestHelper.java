package com.projet.java.javaserver;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

public class FacePPTestHelper {
    public final static String IMAGE64_VALUE = "imacoollookingimage";

    public static HttpEntity buildDetectAPI() {
        MultiValueMap<String, String> headers = mockHeadersWithKeys();
        MultiValueMap<String, String> body = mockBodyWithKeys();
        body.set("image_base64", IMAGE64_VALUE);

        return new HttpEntity<>(body, headers);
    }

    public static MultiValueMap<String, String> mockHeadersWithKeys() {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();

        headers.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED_VALUE);

        return headers;
    }

    public static MultiValueMap<String, String> mockBodyWithKeys() {
        MultiValueMap<String, String> body = new LinkedMultiValueMap<>();

        body.set("api_key", "api_key");
        body.set("api_secret", "api_secret");

        return body;
    }
}
