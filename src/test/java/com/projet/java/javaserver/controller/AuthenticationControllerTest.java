package com.projet.java.javaserver.controller;

import com.projet.java.javaserver.FacePPTestHelper;
import com.projet.java.javaserver.api.facepp.DetectResponse;
import com.projet.java.javaserver.api.facepp.FaceWithConfidence;
import com.projet.java.javaserver.api.facepp.Faces;
import com.projet.java.javaserver.api.facepp.SearchResponse;
import com.projet.java.javaserver.entities.user.UserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.header;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringRunner.class)
@WebMvcTest(AuthenticationController.class)
public class AuthenticationControllerTest {
    @Autowired
    private MockMvc mvc;

    private RestTemplate restTemplateMock = mock(RestTemplate.class);
    @MockBean
    UserRepository userRepository;

    private MockRestServiceServer mockApi;

    @Before
    public void setUp() {
        this.mockApi = MockRestServiceServer.createServer(new RestTemplate());
    }

    @Test
    public void logMeOnGet() throws Exception {
        mvc.perform(get("/auth")).andExpect(status().isNotFound());
    }

    @Test
    public void logMeWithoutBodyFailed() throws Exception {
        mvc.perform(post("/auth")).andExpect(status().isNotFound());
    }

    @Test
    public void logMe() throws Exception {
        //        mockApi.expect(requestTo("https://api-us.faceplusplus.com/facepp/v3/detect"))
        //        .andRespond(
        //                withSuccess("{\"faces\": [{\"face_token\":\"12az\"}]}", MediaType.APPLICATION_JSON));
        //
        //        mockApi.expect(requestTo("https://api-us.faceplusplus.com/facepp/v3/search")).andExpect(method
        // (HttpMethod
        //
        //    .POST)
        //        ).andExpect(
        //                method(HttpMethod.POST)).andRespond(withSuccess("{\"face_token\":\"12az\",
        // \"confidence\":85}",
        //                                                                MediaType.APPLICATION_JSON));
        when(restTemplateMock.postForObject("https://api-us.faceplusplus.com/facepp/v3/detect",
                                            FacePPTestHelper.buildDetectAPI(),
                                            DetectResponse.class)).thenReturn(new DetectResponse(new Faces[]{new Faces(
                "222222")}));
        when(restTemplateMock.postForObject("https://api-us.faceplusplus.com/facepp/v3/search",
                                            FacePPTestHelper.buildDetectAPI(),
                                            SearchResponse.class)).thenReturn(new SearchResponse(
                                                    new FaceWithConfidence[]{
                                                            new FaceWithConfidence("222222", 85F)}));
        mvc.perform(post("/auth/").contentType(MediaType.APPLICATION_JSON).content(
                "{\"image\": \"zhjahjvrehjvajhlvrza\"}")).andExpect(status().isOk());

        System.out.println("bbjpabjaez");
    }
}