package com.projet.java.javaserver.entities.equipment;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class EquipmentTest {

    private Equipment equipment;

    @Before
    public void setUp() {
        this.equipment = new Equipment("tonfa", 5);
    }

    @Test
    public void createEmptyEquipment() {
        this.equipment = new Equipment();
        assertNotNull(this.equipment);
    }

    @Test
    public void getId() {
        this.equipment.setId(1);
        assertEquals(Integer.valueOf(1), this.equipment.getId());
    }

    @Test
    public void getName() {
        assertEquals("tonfa", this.equipment.getName());
    }

    @Test
    public void getBaseAvailable() {
        assertEquals(Integer.valueOf(5), this.equipment.getBaseAvailable());
    }

    @Test
    public void getLeft() {
        assertEquals(Integer.valueOf(5), this.equipment.getLeft());
    }

    @Test
    public void getTaken() {
        assertFalse(this.equipment.getTaken());
    }

    @Test
    public void takeOne() {
        this.equipment.takeOne();
        assertEquals(Integer.valueOf(4), this.equipment.getLeft());
        assertEquals(Integer.valueOf(5), this.equipment.getBaseAvailable());
    }

    @Test
    public void returnOne() {
        this.equipment.returnOne();
        assertEquals(Integer.valueOf(5), this.equipment.getLeft());
        assertEquals(Integer.valueOf(5), this.equipment.getBaseAvailable());
        this.equipment.takeOne();
        this.equipment.returnOne();
        assertEquals(Integer.valueOf(5), this.equipment.getLeft());
        assertEquals(Integer.valueOf(5), this.equipment.getBaseAvailable());
    }

    @Test
    public void isTaken() {
        this.equipment.isTaken();
        assertTrue(this.equipment.getTaken());
    }

    @Test
    public void notTaken() {
        this.equipment.notTaken();
        assertFalse(this.equipment.getTaken());
    }
}