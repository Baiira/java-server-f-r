package com.projet.java.javaserver.entities.took;

import com.projet.java.javaserver.entities.equipment.Equipment;
import com.projet.java.javaserver.entities.user.User;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TookTest {

    private Took took;
    private User user;
    private Equipment equipment;

    @Before
    public void initTest() {
        this.user = new User("f", "t");
        this.equipment = new Equipment("e", 2);
        this.took = new Took(this.user, this.equipment);
    }

    @Test
    public void initEmptyTook() {
        this.took = new Took();
        assertNotNull(this.took);
    }

    @Test
    public void getId() {
        this.took.setId(1);
        assertEquals(Integer.valueOf(1), this.took.getId());
    }

    @Test
    public void getUser() {
        assertEquals(this.user, this.took.getUser());
    }

    @Test
    public void getEquipment() {
        assertEquals(this.equipment, this.took.getEquipment());
    }
}