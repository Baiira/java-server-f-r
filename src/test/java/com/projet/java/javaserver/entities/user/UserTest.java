package com.projet.java.javaserver.entities.user;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class UserTest {
    private User user;

    @Before
    public void initTest() {
        this.user = new User();
    }

    @Test
    public void createUserWithParam() {
        String lName = "jeandral";
        String fName = "fred";

        this.user = new User(fName, lName);

        assertEquals(fName, this.user.getFirstName());
        assertEquals(lName, this.user.getLastName());
        assertNull(this.user.getId());
        assertNull(this.user.getFaceToken());
    }

    @Test
    public void getId() {
        this.user.setId(1);
        assertEquals(Integer.valueOf(1), this.user.getId());
    }

    @Test
    public void getFirstName() {
        String fName = "fred";
        this.user.setFirstName(fName);
        assertEquals(fName, this.user.getFirstName());
    }

    @Test
    public void getLastName() {
        String lName = "jeandral";
        this.user.setLastName(lName);
        assertEquals(lName, this.user.getLastName());
    }

    @Test
    public void getFaceToken() {
        String faceToken = "some huge string";
        this.user.setFaceToken(faceToken);
        assertEquals(faceToken, this.user.getFaceToken());
    }
}